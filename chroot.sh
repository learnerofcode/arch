#!/bin/bash

# IMPORTANT: Change these values before running this script

HOST_NAME=archbox
ROOT_PASS=changeme
NON_ROOT_USER=changeme
NON_ROOT_USER_PASS=changeme
USING_VIRTUALBOX=true


echo "Setting the time zone..."
ln -sf /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime


echo "Setting up the hardware clock..."
hwclock --systohc


echo "Setting the locale..."
echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
locale-gen
export LANG=en_US.UTF-8
echo "LANG=en_US.UTF-8" >> /etc/locale.conf


echo "Setting the hostname..."
echo $HOST_NAME > /etc/hostname


echo "Setting up the hosts file..."
cat << CONF > /etc/hosts
127.0.0.1 localhost
::1 localhost
127.0.1.1 $HOST_NAME.localdomain $HOST_NAME
CONF


echo "Setting the root password..."
echo "root:${ROOT_PASS}" | chpasswd


# echo "Installing Linux LTS Kernel..."
# pacman --noconfirm -S linux-lts linux-lts-headers

echo "Installing linux-headers"
pacman --noconfirm -S linux-headers

echo "Installing the bootloader..."
bootctl install


echo "Configuring the bootloader..."
cat << CONF > /boot/loader/entries/arch.conf
title Arch Linux
linux /vmlinuz-linux
initrd /initramfs-linux.img
options root=PARTUUID=$(blkid -s PARTUUID -o value /dev/sda3) rw
CONF


echo "Adding the non-root user..."
useradd -m -g users -G wheel,audio,video,storage,optical,power -s /bin/bash $NON_ROOT_USER
echo "${NON_ROOT_USER}:${NON_ROOT_USER_PASS}" | chpasswd


echo "Making the non-root user a sudoer..."
echo $NON_ROOT_USER ALL=\(ALL\) NOPASSWD: ALL >> /etc/sudoers


echo "Enabling pacman multilib repo..."
sed -i "/^#\[multilib\]$/,+1{s/^#//}" /etc/pacman.conf


echo "Doing a full system update..."
pacman --noconfirm -Syyu

if [ $USING_VIRTUALBOX ]; then
	echo "Installing virtualbox guest additions..."
	pacman --noconfirm -S virtualbox-guest-utils
	systemctl enable vboxservice.service
else
	echo "Installing nvidia drivers..."
	pacman --noconfirm -S nvidia-dkms \
		libglvnd \
		nvidia-utils \
		opencl-nvidia \
		lib32-nvidia-utils \
		lib32-opencl-nvidia \
		nvidia-settings


	echo "Use nvidia kernel DRM modules"
	sed -i "s/MODULES=()/MODULES=(nvidia nvidia_modeset nvidia_uvm nvidia_drm)/g" /etc/mkinitcpio.conf
	sed -i "$ s/$/ nvidia-drm.modeset=1/" /boot/loader/entries/arch.conf


	echo "Creating pacman mkinitcpio hook..."
	mkdir -p /etc/pacman.d/hooks
	cat << HOOK > /etc/pacman.d/hooks/nvidia.hook
[Trigger]
Operation=Install
Operation=Upgrade
Operation=Remove
Type=Package
Target=nvidia-dkms
Target=linux
# Change the linux part above and in the Exec line if a different kernel is used

[Action]
Description=Update Nvidia module in initcpio
Depends=mkinitcpio
When=PostTransaction
NeedsTargets
Exec=/bin/sh -c 'while read -r trg; do case \$trg in linux) exit 0; esac; done; /usr/bin/mkinitcpio -P'
HOOK
fi


echo "Installing programs..."
pacman --noconfirm -S alsa-utils \
	chromium \
	cmatrix \
	code \
	dmenu \
	docker \
	dunst \
	emacs \
	fd \
	feh \
	file-roller \
	firefox \
	fzf \
	git \
	go \
	gvfs \
	htop \
	i3-gaps \
	i3lock \
	i3status \
	ibus \
	ibus-unikey \
	irssi \
	jdk8-openjdk \
	jq \
	kotlin \
	leafpad \
	libnotify \
	lxappearance \
	mpc \
	mpd \
	mpv \
	ncmpcpp \
	neofetch \
	neovim \
	networkmanager \
	newsboat \
	nodejs \
	npm \
	openssh \
	p7zip \
	picom \
	postgresql \
	pavucontrol \
	playerctl \
	pulseaudio \
	pulseaudio-alsa \
	python \
	python-pip \
	python-pywal \
	ranger \
	redis \
	redshift \
	reflector \
	ripgrep \
	rofi \
	rxvt-unicode \
	scrot \
	the_silver_searcher \
	thunar \
	thunar-archive-plugin \
	tmux \
	transmission-gtk \
	ttf-arphic-uming \
	ttf-baekmuk \
	ttf-cascadia-code \
	ttf-dejavu \
	ttf-droid \
	ttf-freefont \
	ttf-inconsolata \
	ttf-joypixels \
	ttf-liberation \
	ttf-ubuntu-font-family \
	tamsyn-font \
	tumbler \
	unrar \
	unzip \
	vim \
	w3m \
	wget \
	xorg-apps \
	xorg-server \
	xorg-xinit \
	xsel \
	yarn \
	zathura \
	zathura-djvu \
	zathura-pdf-mupdf \
	zsh


echo "Installing yay - an AUR helper..."
cd /home/$NON_ROOT_USER
git clone --depth=1 https://aur.archlinux.org/yay.git
chown $NON_ROOT_USER yay
cd yay
su $NON_ROOT_USER -c "yes | makepkg -sri"


echo "Installing additional packages from AUR..."
su $NON_ROOT_USER -c "yay --noconfirm --noeditmenu --nodiffmenu --nocleanmenu -S cava \
	polybar \
	siji-git \
	spotify \
	ttf-mononoki-git \
	ttf-weather-icons \
	youtube-dl \
	zeal"


echo "Changing user shell to zsh..."
chsh $NON_ROOT_USER -s $(which zsh)


echo "PostgreSQL initdb..."
su postgres -c "initdb -D '/var/lib/postgres/data'"


echo "Increase the amount of inotify watchers..."
echo "fs.inotify.max_user_watches=524288" >> /etc/sysctl.d/99-sysctl.conf


echo "Enabling services..."
systemctl enable NetworkManager
systemctl enable postgresql
systemctl enable redis

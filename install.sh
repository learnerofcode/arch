#!/bin/bash

SWAP_SIZE=4G


echo "Checking for UEFI..."
efivar -l >/dev/null 2>&1
if [[ $? -ne 0 ]]; then
	echo "ERROR: UEFI not detected, exiting..."
	exit 0
fi
echo "UEFI detected!"
reset


echo "Update the system clock..."
timedatectl set-ntp true


echo "Partitioning the disk..."

cat <<EOF | fdisk /dev/sda
g
n


+512M
t
1
n


+${SWAP_SIZE}
n



w
EOF


echo "Formatting the partitions..."
yes | mkfs.fat -F32 /dev/sda1
mkswap /dev/sda2
swapon /dev/sda2
yes | mkfs.ext4 /dev/sda3


echo "Mounting the file systems..."
mount /dev/sda3 /mnt
mkdir -p /mnt/boot
mount /dev/sda1 /mnt/boot


echo "Installing the base system..."
pacstrap -i /mnt base base-devel \
	cryptsetup \
	device-mapper \
	dhcpcd \
	diffutils \
	e2fsprogs \
	inetutils \
	jfsutils \
	less \
	linux \
	linux-firmware \
	logrotate \
	lvm2 \
	man-db \
	man-pages \
	mdadm \
	nano \
	netctl \
	perl \
	reiserfsprogs \
	s-nail \
	sysfsutils \
	texinfo \
	usbutils \
	vi \
	which \
	xfsprogs


echo "Generating fstab..."
genfstab -U /mnt >> /mnt/etc/fstab


arch-chroot /mnt
